//title of the whole website
$("#select").on("mouseover", function () {
    $("#select").text("ኢትዮጵያ").css("color", "black");
});

$("#select").on("mouseout", function () {
    $("#select").text("Ethiopia").css("color", "white");
});

// navbar home button
$(".name").on("mouseover", function () {
    $(".name").text("ቤት");
});

$(".name").on("mouseout", function () {
    $(".name").text("Home");
});

// navbar coffee button
$(".name1").on("mouseover", function () {
    $(".name1").text("ቡና");
});

$(".name1").on("mouseout", function () {
    $(".name1").text("Coffee");
});

// navbar food button
$(".name2").on("mouseover", function () {
    $(".name2").text("ምግብ");
});

$(".name2").on("mouseout", function () {
    $(".name2").text("Food");
});

// navbar culture button
$(".name3").on("mouseover", function () {
    $(".name3").text("ባህል");
});

$(".name3").on("mouseout", function () {
    $(".name3").text("Culture");
});


// navbar nature button
$(".name4").on("mouseover", function () {
    $(".name4").text("ተፈጥሮ");
});

$(".name4").on("mouseout", function () {
    $(".name4").text("Nature");
});


// navbar Landmarks button
$(".name5").on("mouseover", function () {
    $(".name5").text("ጥንታዊ ሕንፃዎች");
});

$(".name5").on("mouseout", function () {
    $(".name5").text("Landmarks");
});

// navbar About Ethiopia button
$(".name6").on("mouseover", function () {
    $(".name6").text("ስለ ኢትዮጵያ");
});

$(".name6").on("mouseout", function () {
    $(".name6").text("About Ethiopia");
});


// navbar Portfolio Plan button
$(".name7").on("mouseover", function () {
    $(".name7").text("ፖርትፎሊዮ ዕቅድ");
});

$(".name7").on("mouseout", function () {
    $(".name7").text("Portfolio Plan");
});
